
import 'package:data_connection_checker_tv/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:user_list/controllers/user_details_by_id_controller.dart';
import 'package:user_list/models/paginated_userlist_model.dart';

import '../component/loading_card.dart';

class UserDetailsPage extends StatefulWidget {
  final UserData userData;
  UserDetailsPage(this.userData,{Key? key}) : super(key: key);

  @override
  _UserDetailsPageState createState() => _UserDetailsPageState();
}

class _UserDetailsPageState extends State<UserDetailsPage> {

   UserDetailsByIdController  userDetailsByIdController= Get.put(UserDetailsByIdController());
  Future<bool?> check_internet()async{

    //print("The statement 'this machine is connected to the Internet' is: ");
    //print(await DataConnectionChecker().hasConnection);
    // returns a bool

    // We can also get an enum instead of a bool
    //print("Current status: ${await DataConnectionChecker().connectionStatus}");
    // ignore: unrelated_type_equality_checks

    // prints either DataConnectionStatus.connected
    // or DataConnectionStatus.disconnected

    // This returns the last results from the last call
    // to either hasConnection or connectionStatus
    //print("Last results: ${DataConnectionChecker().lastTryResults}");

    // actively listen for status updates

    var listener = DataConnectionChecker().onStatusChange.listen((status) {
      switch (status) {
        case DataConnectionStatus.connected:
      //    print('Data connection is available.');
          // Get.off(() => Slider_class());
          Get.off( UserDetailsPage(widget.userData));
          //return true;
          break;
        case DataConnectionStatus.disconnected:{
        //  print('You are disconnected from the internet.');

          Get.defaultDialog(

              title: "",
              content:const Text('check_your_internet_connection',textScaleFactor: 1.0,
                  style: TextStyle(
                    fontFamily:'ARBFONTS-TAJAWAL-REGULAR-2',
                    color: Color(0xff888888),
                    fontSize:16 ,
                  )) ,
              /* middleText: translator.translate('check_your_internet_connection',),
        middleTextStyle: ,*/
              actions: [
                Center(
                  child: Container(
                    //  width: 290 / 360 * width,
                    //height: 36 / 640 * height,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: const Color(0xffFF7335),
                    ),
                    child: TextButton(
                     
                      child:const Text('ok',textScaleFactor: 1.0,
                          style:  TextStyle(
                            fontFamily:'ARBFONTS-TAJAWAL-REGULAR-2',
                            color: Colors.white,
                            fontSize:16 ,
                          )) ,
                      // textColor: Colors.black,
                      onPressed: () {
                        check_internet();
                      //  Get.back();
                      },
                    ),
                  ),
                ),
              ]);




          break;
        }}
    });

    // close listener after 30 seconds, so the program doesn't run forever
    await Future.delayed(const Duration(seconds: 30));
    await listener.cancel();
  }

  @override
  void initState() {
    super.initState();
    check_internet();
      userDetailsByIdController.fetchUserDetailsDetails(widget.userData.id!);
   

  }

  Widget build(BuildContext context) {
    var height = (MediaQuery.of(context).size.height);
    var width = (MediaQuery.of(context).size.width);
    var size = (MediaQuery.of(context).size) * 0.01;
   // final textScale = MediaQuery.of(context).textScaleFactor;
    final textScale=1.0;
    return Scaffold(
      appBar: AppBar(
                //elevation: 5,
                 leading: Padding(
                padding: EdgeInsets.only(
                    left: 10 / 360 * width, top: 0 / 760 * height),
                child: IconButton(
                  icon: Icon(Icons.arrow_back_outlined,color: Color(0xff6dddf8)),
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
                centerTitle: true,
                title:  Text(
                                  'User Details',

                   textScaleFactor: 1.0,
                      overflow: TextOverflow.ellipsis,
                   style: TextStyle(
                      fontFamily: 'SomarRounded-Medium',
                      color:const  Color(0xff555555),
                      fontSize: 16 * textScale,
                    )
                ),
                shape: const RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.vertical(bottom: Radius.circular(8))),
                backgroundColor: Colors.white,
                //  shadowColor: Color(0xff1B8CA27D),
      ),
              
      body: Obx(() {
         if (userDetailsByIdController.isLoading.value) {
            return Center(child:const CircularProgressIndicator());
      }
      else{
        if(userDetailsByIdController.user!=null){
            return SingleChildScrollView(child: Padding(
              padding: const EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                SizedBox(height: 40/760*height,),
                                      Center(
                              child: Container(
                                height: 125 / 360 * width,
                                width: 125 / 360 * width,

                                decoration: BoxDecoration(
                               
                                  borderRadius: BorderRadius.circular(100),
                                ),
                                child:  CircleAvatar(
                                        //radius: 60/360 * width,
                                        backgroundColor:Colors.grey[350],
                                        backgroundImage: NetworkImage(
                                          userDetailsByIdController.user!.data!.avatar.toString()
                                            
                                )),
                              ),),
                              SizedBox(height: 80/760*height,),
 Row(
                  children: [
                   
                  Container(
                  width: 90/360*width,
                   
                      child: Text(
                        'User Id:',
                     
                          textScaleFactor: 1.0,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                            //fontFamily: 'SomarRounded-Regular',
                            color: const Color(0xff6dddf8),
                            fontSize: 14 * textScale,
                          )),
                    ),
                    SizedBox(width: 10 / 360 * width),
                   Container(
                          constraints: BoxConstraints(maxWidth: 210/360*width,),
                      child: Text(
                        widget.userData.id .toString(),
                       
                        textScaleFactor: 1.0,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                            fontFamily: 'SomarRounded-Medium',
                            color: const Color(0xff555555),
                            fontSize: 14 * textScale,
                          )),
                    ),
                    SizedBox(width: 10 / 360 * width),
                  ],
                ),


                              SizedBox(height: 20/760*height,),
                                Row(
                  children: [
                   
                  Container(
                  width: 90/360*width,
                   
                      child: Text(
                        'First Name:',
                     
                          textScaleFactor: 1.0,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                            //fontFamily: 'SomarRounded-Regular',
                            color: const Color(0xff6dddf8),
                            fontSize: 14 * textScale,
                          )),
                    ),
                    SizedBox(width: 10 / 360 * width),
                   Container(
                          constraints: BoxConstraints(maxWidth: 210/360*width,),
                      child: Text(
                        widget.userData.firstName ?? "",
                       
                        textScaleFactor: 1.0,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                            fontFamily: 'SomarRounded-Medium',
                            color: const Color(0xff555555),
                            fontSize: 14 * textScale,
                          )),
                    ),
                    SizedBox(width: 10 / 360 * width),
                  ],
                ),

SizedBox(height: 20/760*height,),
 Row(
                  children: [
                   
                  Container(
                  width: 90/360*width,
                   
                      child: Text(
                        'Last Name:',
                     
                          textScaleFactor: 1.0,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                            //fontFamily: 'SomarRounded-Regular',
                            color: const Color(0xff6dddf8),
                            fontSize: 14 * textScale,
                          )),
                    ),
                    SizedBox(width: 10 / 360 * width),
                   Container(
                          constraints: BoxConstraints(maxWidth: 210/360*width,),
                      child: Text(
                        widget.userData.lastName ?? "",
                       
                        textScaleFactor: 1.0,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                            fontFamily: 'SomarRounded-Medium',
                            color: const Color(0xff555555),
                            fontSize: 14 * textScale,
                          )),
                    ),
                    SizedBox(width: 10 / 360 * width),
                  ],
                ),
                SizedBox(height: 20/760*height,),
                 Row(
                  children: [
                   
                  Container(
                  width: 90/360*width,
                   
                      child: Text(
                        'Email:',
                     
                          textScaleFactor: 1.0,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                            //fontFamily: 'SomarRounded-Regular',
                            color: const Color(0xff6dddf8),
                            fontSize: 14 * textScale,
                          )),
                    ),
                    SizedBox(width: 10 / 360 * width),
                   Container(
                          constraints: BoxConstraints(maxWidth: 210/360*width,),
                      child: Text(
                        widget.userData.email ?? "",
                       
                        textScaleFactor: 1.0,
                          overflow: TextOverflow.clip,
                          style: TextStyle(
                            fontFamily: 'SomarRounded-Medium',
                            color: const Color(0xff555555),
                            fontSize: 14 * textScale,
                          )),
                    ),
                    SizedBox(width: 10 / 360 * width),
                  ],
                ),


        ]),
            ),);
        }
        else{
        return  Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
          Text('somthing went wrong')

        ]);
        }
      
      }
      })
    );
  }
}