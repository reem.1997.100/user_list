import 'package:data_connection_checker_tv/data_connection_checker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Trans;
import 'package:user_list/controllers/paginated_userList_controller.dart';
import 'package:user_list/models/paginated_userlist_model.dart';
import 'package:user_list/pages/user_details_page.dart';

import '../component/loading_card.dart';
import '../component/user_card.dart';


class UsersListPage extends StatefulWidget {

  UsersListPage({Key? key}) : super(key: key);

  @override
  _UsersListPageState createState() => _UsersListPageState();
}

class _UsersListPageState extends State<UsersListPage> {

  bool searching=false;
  
  List<UserData>? users;
  bool error=false;
  Future<bool?> check_internet() async {
    //print("The statement 'this machine is connected to the Internet' is: ");
    //print(await DataConnectionChecker().hasConnection);
    // returns a bool

    // We can also get an enum instead of a bool
   // print("Current status: ${await DataConnectionChecker().connectionStatus}");
    // ignore: unrelated_type_equality_checks

    // prints either DataConnectionStatus.connected
    // or DataConnectionStatus.disconnected

    // This returns the last results from the last call
    // to either hasConnection or connectionStatus
    //print("Last results: ${DataConnectionChecker().lastTryResults}");

    // actively listen for status updates

    var listener = DataConnectionChecker().onStatusChange.listen((status) {
      switch (status) {
        case DataConnectionStatus.connected:
         // print('Data connection is available.');
          Get.off(UsersListPage());
         
          break;
        case DataConnectionStatus.disconnected:
          {
           // print('You are disconnected from the internet.');

            Get.defaultDialog(
                title: "",
                content:
                   const Text('check_your_internet_connection',
                        textScaleFactor: 1.0,
                        style:  TextStyle(
                       
                          color: Color(0xff888888),
                          fontSize: 16,
                        )),
          
                actions: [
                  Center(
                    child: Container(
             
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(0xffFF7335),
                      ),
                      child: TextButton(
                      
                        child:const  Text('ok',
                            textScaleFactor: 1.0,
                            style:  TextStyle(
                    color: Colors.white,
                              fontSize: 16,
                            )),
                        // textColor: Colors.black,
                        onPressed: () {
                          check_internet();
                          // Get.back();
                        },
                      ),
                    ),
                  ),
                ]);

            //return false;
            break;
          }
      }
    });

    // close listener after 30 seconds, so the program doesn't run forever
    await Future.delayed(Duration(seconds: 10));
    await listener.cancel();
  }

  @override
  void initState() {
       check_internet();
    super.initState();
     
    users = [];


  }
    final PaginatedUserListControler paginatedUserListControler=
  Get.put(PaginatedUserListControler());

  void getSuggestion(String text) async {
 users =   
                      paginatedUserListControler.userListdata!
        .where((element) =>
            element.id.toString()==text
            )
        .toList();
    if(mounted){
    setState(() {
  
    //  users!.clear();
      
     
        if(users!.length!=0){
      
  print(users![0].firstName);
      searching = true;
      error=false;}
      else{
        if(text.isEmpty){
            searching=false;
        }
        else{
          searching=true;
        }
      
     
      }
    });
    }
        
  
   
  }
  Widget build(BuildContext context) {
  
    var height = (MediaQuery.of(context).size.height);
    var width = (MediaQuery.of(context).size.width);
    var size = (MediaQuery.of(context).size) * 0.01;
    final textScale=1.0;
    // final textScale = MediaQuery.of(context).textScaleFactor;


    return Scaffold(
       appBar: PreferredSize(
          preferredSize: Size.fromHeight(85 / 760 * height),
          child: Container(
              decoration: const BoxDecoration(boxShadow: [
                BoxShadow(
                  color:  Color(0xff1B8CA27D),
                  offset: Offset(0, 0),
                  blurRadius: 16.0,
                )
              ]),
              child: AppBar(
                //elevation: 5,
             
                centerTitle: true,
                title:  Text(
                   'Users',            
                   style:  TextStyle(
                      color:const  Color(0xff555555),
                    fontSize: 16 * textScale,
                    )
                ),
                   flexibleSpace: Container
              (
               // color: Colors.amber,
               padding: EdgeInsets.only(top:70/760*height),
               child:  Stack(
              children: [
                Container(
                 // color: Color(0xff6DDDF8),
                  height: 44 / 760 * height,
                  decoration:  BoxDecoration(
                     borderRadius: BorderRadius.circular(8.0),
                    color: Colors.blueAccent,
                  ),
                  //  height: 30 / 640 * height,
                ),
                
                searchField(),
                ])),
                shape: const RoundedRectangleBorder(
                    borderRadius:
                        BorderRadius.vertical(bottom: Radius.circular(8))),
                backgroundColor: Colors.white,
                //  shadowColor: Color(0xff1B8CA27D),
                
              ))),
    body: SingleChildScrollView(
      child: Container(
          color: Colors.white,
         height: height,
         // color:Color(0xffF6F6F6),// Colors.white,
           width: width,
           child:  Container(
           
                      // color: Colors.lightBlue,
                      // height: 284/640*height,
                      //  margin: const EdgeInsets.only(left: 0.0, right: 0.0),
                      //color: Colors.white,
                      child:(!searching&&!error)?Obx(() {
                       
                        if (paginatedUserListControler.isLoading.value &&
                           paginatedUserListControler.first_time_for_user_list.value) {
                          return
                              //LoadingCard(100,100),
                              ListView.builder(
                                  padding: EdgeInsets.only(
                                    top: 20 / 760 * height,
                                    bottom: 50 / 760 * height,
                                    left: 15 / 360 * width,
                                    right: 15 / 360 * width,
                                  ),
                                  scrollDirection: Axis.vertical,
                                  physics: ScrollPhysics(),
                                  shrinkWrap: true,
                                  primary: true,
                                  itemCount: 6,
                            
                                  itemBuilder: (context, index) {
                                    {
                                      return LoadingCard(
                                          100 / 360 * width, 100 / 360 * width);
                                    }
                                  });
                        } else {
                          if (paginatedUserListControler
                                  .paginationUserListModel !=
                              null) {
                               paginatedUserListControler.changeFirstTimeValue(false);
                     
                           if ( paginatedUserListControler
                                    .paginationUserListModel!
                                    .page!
                                     >
                                paginatedUserListControler
                                    .paginationUserListModel!
                                    .totalPages!)
                               paginatedUserListControler
                                  .paginationUserListModel = null;
                          } else
                           
                        print('Model is null');
                          return Column(
                            children: [
                              Stack(children: [
                                Container(
                                  //color: Colors.amber,
                                  height: 660 / 760 * height,
                                  child:
                                      NotificationListener<ScrollNotification>(
                                    onNotification:
                                        (ScrollNotification scrollInfo) {
                                      if (! paginatedUserListControler
                                              .isLoading.value &&
                                          scrollInfo.metrics.pixels ==
                                              scrollInfo
                                                  .metrics.maxScrollExtent) {
                                        if ( paginatedUserListControler
                                                .paginationUserListModel !=
                                            null) {
                                          //print('next != null');
                                          paginatedUserListControler
                                              .fetchpaginationsubcategory();
                                          //  subcategories.add( pagination_subcategory_controller.pagination_subCategory_Model);
                                          //  subcategorydata.addAll(pagination_subcategory_controller.pagination_subCategory_Model.subcategoryData);
      
                                        } else {
                                       //   print('next is null');
                                        }
                                      }
      
                                      return true;
                                    },
                                    child: ListView.builder(
                                          padding: EdgeInsets.only(
                                    right: 0 / 360 * width,
                                    left: 0 / 360 * width,
                                    bottom: 20 / 760 * height),
                                        scrollDirection: Axis.vertical,
                                    
                                        shrinkWrap: true,
                                        primary: true,
                                        itemCount:
                                          paginatedUserListControler
                                                .userListdata!.length,
                                        //subcategorydata.length,
                                     
                                          // childAspectRatio:(100/360*width)/(114/640*height),
                                        
                                        itemBuilder: (context, index) {
                                          {
                                            return GestureDetector(
                                              child: 
                                          
                                          GestureDetector(
                                            onTap: (){Get.to(UserDetailsPage(paginatedUserListControler.userListdata![index]));},
                                            child: UserCard(paginatedUserListControler.userListdata![index])),
                                              onTap: () {
                                                //print( 'go to Medicine_of_Categories_Page');
                                               
                                              },
                                            );
                                          }
                                        }),
                                  ),
                                ),
                                if ( paginatedUserListControler
                                    .isLoading.value)
                                  Positioned(
                                    bottom: 0,
                                    right: 150 / 360 * width,
                                    child: Container(
                                      //height: pagination_product_controller.isLoading.value? 50.0 : 0,
                                      color: Colors.transparent,
                                      child: Align(
                                        alignment: Alignment.bottomCenter,
                                        child: Center(
                                          child:
                                              paginatedUserListControler
                                                      .isLoading.value
                                                  ? Theme(
                                                      data: Theme.of(context)
                                                          .copyWith(
                                                        accentColor:
                                                            Color(0xff6dddf8),
                                                      ),
                                                      child:
                                                          new CircularProgressIndicator(),
                                                    )
                                                  : SizedBox(),
                                        ),
                                      ),
                                    ),
                                  ),
                              ]),
                            ],
                          );
                              }
                      })
                              // when search
                              :
                                  (users!.isNotEmpty)?
                                     ListView.builder(
                                          padding: EdgeInsets.only(
                                    right: 0 / 360 * width,
                                    left: 0 / 360 * width,
                                    bottom: 20 / 760 * height),
                                        scrollDirection: Axis.vertical,
                                    
                                        shrinkWrap: true,
                                        primary: true,
                                        itemCount:
                                         users!.length,
                                        //subcategorydata.length,
                                     
                                          // childAspectRatio:(100/360*width)/(114/640*height),
                                        
                                        itemBuilder: (context, index) {
                                          {
                                            return GestureDetector(
                                              child: 
                                          
                                          GestureDetector(
                                            onTap: (){Get.to(UserDetailsPage(users![index]));},
                                            child: UserCard(users![index])),
                                              onTap: () {
                                                //print( 'go to Medicine_of_Categories_Page');
                                               
                                              },
                                            );
                                          }
                                        })
                                        
                                  
                                :Container(child:Center(child:Text('No Result')))
                              
                    )
                ,
       
      ),
    )

      
      );
      
      
   
}
 Widget searchField() {
    //search input field
    var height = (MediaQuery.of(context).size.height);
    var width = (MediaQuery.of(context).size.width);
       final textScale = 1.0;
    return Column(
      children: [
        Container(
            height: 34/ 760 * height,
            width: 260 / 360 * width,
            margin: EdgeInsets.only(
                top: 5 / 760 * height,
                left: 50 / 360 * width,
                right: 50 / 360 * width),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4.0),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                  color: Color(0xff000000).withOpacity(0.35),
                  spreadRadius: 0,
                  blurRadius: 4,
                  offset: Offset(0, 1), // changes position of shadow
                ),
              ],
            ),
            child: 
            Container(
                  height: 34 / 760 * height,
                  width: 260 / 360 * width,
                  decoration:
                      BoxDecoration(borderRadius: BorderRadius.circular(9)),
                  child: TextFormField(
                   
                   onChanged: (value) {
                   
                    print('in search');
                    getSuggestion(value);
                  },
                  
                    onSaved: (e) {},
                    style:   TextStyle(
                  
                        color: Color(0xff555555),
                        fontSize: 14*textScale),
                    decoration: InputDecoration(
                       border: InputBorder.none,
                        isCollapsed: true,
                         contentPadding:  EdgeInsets.only(top:5/760*height,
                         left: 10/360*width,right:  10/360*width,bottom: 0/760*height),
                     
                  hintStyle: TextStyle(
                
                        color: Colors.grey,
                        fontSize: 14*textScale),
                    hintText:  'search',
                 
                    
                   
                    ),
             
                  ),
                ),
            ),
      ],
    );
  }


}
