
import 'package:get/get.dart';
import 'package:user_list/api/user_api.dart';
import 'package:user_list/models/paginated_userlist_model.dart';

class PaginatedUserListControler extends GetxController {
  var isLoading = false.obs;
   var first_time_for_user_list = true.obs;

  PaginatedUserListModel? paginationUserListModel=PaginatedUserListModel();
 
   PaginatedUserListModel? pagination_subCategory_Model_for_firstPage=PaginatedUserListModel();
List <UserData> ?userListdata=[];
//List <UserData> ?userListdatafirstPage=[];

  UserApi userApi=UserApi();
int page=1;

  @override
  void onInit() {
   userListdata!.clear();
    fetchpaginationsubcategory();
    super.onInit();
  }

  Future changeFirstTimeValue(bool val ) async {

   
   first_time_for_user_list(val);
  
   }

  Future<bool> fetchpaginationsubcategory() async {

isLoading(true);
    paginationUserListModel=null;

    paginationUserListModel= await userApi.GetPaginatedUserList('https://reqres.in/api/users?page='+page.toString());
    update();
    if (paginationUserListModel == null) {
      return false;
    } else {
          userListdata!
                                .addAll(
                                  paginationUserListModel!.data!);
                                  page=page+1;
      isLoading(false);
      update();
      return true;
    }
  }

  


}
