
import 'package:get/state_manager.dart';
import 'package:user_list/api/user_api.dart';
import 'package:user_list/models/user_details_model.dart';

class UserDetailsByIdController extends GetxController {
  var isLoading = true.obs;


  UserDetailsModel ?user =UserDetailsModel();



  UserApi userApi=UserApi();

  @override
  void onInit() {

    super.onInit();
  }

  Future<bool> fetchUserDetailsDetails(int id) async {
    isLoading(true);
    user = await userApi.GetUserDetailsById(id);
    update();
    if (user == null) {

      return false;
    } else {

      isLoading(false);
      update();
      return true;
    }
  }

}
