import 'package:shimmer/shimmer.dart';
import 'package:flutter/material.dart';

class LoadingCard extends StatelessWidget {
  double w;
  double h;
  LoadingCard(this.w, this.h, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var height = (MediaQuery.of(context).size.height);
    var width = (MediaQuery.of(context).size.width);
    return Shimmer.fromColors(
      period: Duration(milliseconds: 700),
      baseColor: Colors.grey[400] as Color ,
      highlightColor: Colors.grey[350] as Color,
      child: Container(
          width: w ,
          height: h ,
          decoration: BoxDecoration(  borderRadius: BorderRadius.circular(10.0),),
          child: Card(
              child: Stack(children: <Widget>[
                Container(
                  // color: Colors.grey,
                  height: double.infinity,
                  width: double.infinity,
                ),
                /*   Positioned(
                  top: 5,
                  left: 5,
                  child: Padding(
                      padding: EdgeInsets.all(2),
                      child: CircleAvatar(
                        backgroundColor: Colors.red,
                        radius: 16,
                      )),
                )*/
              ]))),
    );
  }
}
