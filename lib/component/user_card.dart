
// ignore_for_file: camel_case_types

import 'package:flutter/material.dart';
import 'package:get/get.dart' hide Trans ;
import 'package:user_list/models/paginated_userlist_model.dart';


class UserCard extends StatefulWidget {
  UserData userData;

  UserCard(
    this.userData, {
    Key? key,
  }) : super(key: key);

  @override
  _UserCardState createState() => _UserCardState();
}

class _UserCardState extends State<UserCard> {
  var isFavorite = false.obs;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var height = (MediaQuery.of(context).size.height);
    var width = (MediaQuery.of(context).size.width);
    var size = (MediaQuery.of(context).size) * 0.01;
    // final textScale = MediaQuery.of(context).textScaleFactor;
    final textScale = 1.0;

    return Padding(
      padding: const EdgeInsets.all(15),
      child: Container(
        padding: EdgeInsets.zero,
        width: 330 / 360 * width,
        height: 100 / 760 * height,
        margin: EdgeInsets.only(top: 10 / 760 * height),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6.0),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: const Color(0xff555555).withOpacity(0.29),
              spreadRadius: 0,
              blurRadius: 12,
              offset:const Offset(0, 6), // changes position of shadow
            ),
          ],
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.zero,
              width: 330 / 360 * width,
              height: 35 / 760 * height,
              decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(6.0),
                      topRight: Radius.circular(6.0)),
                  color:Colors.white,),
              child: Center(
                child: Text(
               
                  "User Id"+
                        ' # ' +
                        widget.userData.id.toString(),
                    textScaleFactor: 1.0,
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                   
                      color: const Color(0xff555555),
                      fontSize: 14 * textScale,
                    )),
              ),
            ),
               Divider(color: Colors.grey, height: 0),
            Expanded(
              child: Padding(
              padding:  EdgeInsets.only(top: 12/760*height),
                child: Row(
                  children: [
                    
                  Container(
          
                      color: Colors.grey[350],
                  
                    child: 
                    Image.network(    widget.userData.avatar.toString()),
            
                  ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                       
                        Row(
                          children: [
                            SizedBox(width: 10 / 360 * width),
                        
                          Container(
                          width: 80/360*width,
                           
                              child: Text(
                                'First Name:',
                             
                                  textScaleFactor: 1.0,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    //fontFamily: 'SomarRounded-Regular',
                                    color: const Color(0xff555555),
                                    fontSize: 14 * textScale,
                                  )),
                            ),
                            SizedBox(width: 10 / 360 * width),
                           Container(
                                  constraints: BoxConstraints(maxWidth: 180/360*width,),
                              child: Text(
                                widget.userData.firstName ?? "",
                               // 'llllllllllllllllljjjjjjjjjjjjjjjjjjjjjjjjjjjjhuhhhhhhhhhhhhhhhhh',
                                textScaleFactor: 1.0,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontFamily: 'SomarRounded-Medium',
                                    color: const Color(0xff555555),
                                    fontSize: 14 * textScale,
                                  )),
                            ),
                            SizedBox(width: 10 / 360 * width),
                          ],
                        ),
                        SizedBox(height: 8 / 760 * height),
                        Row(
                          children: [
                           
                            SizedBox(width: 10 / 360 * width),
                            Container(
                          width: 80/360*width,
                           
                              child: Text(
                               // 'llllllllllllkkkkkkkkkkkkkkkk',
                              'Last Name:',
                                  textScaleFactor: 1.0,
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    
                                    color: const Color(0xff555555),
                                    fontSize: 14 * textScale,
                                  )),
                            ),
                            SizedBox(width: 10 / 360 * width),
                           Container(
                                  constraints: BoxConstraints(maxWidth: 180/360*width,),
                              child: Text(
                                //'lllllllllllllllhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh',
                                widget.userData.lastName ?? "",
                                  textScaleFactor: 1.0,
            
                                  overflow: TextOverflow.ellipsis,
                                  style: TextStyle(
                                    fontFamily: 'SomarRounded-Medium',
                                    color:const  Color(0xff555555),
                                    fontSize: 14 * textScale,
                                  )),
                            ),
                            SizedBox(width: 10 / 360 * width),
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: 15/760*height,)
          ],
        ),
      ),
    );
  }
}
