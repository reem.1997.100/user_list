// ignore_for_file: avoid_print, non_constant_identifier_names, body_might_complete_normally_nullable, unused_local_variable, prefer_const_declarations, file_names

import 'dart:async';

import 'dart:convert';
import 'package:http/http.dart' ;
import 'package:user_list/models/user_details_model.dart';

import '../models/paginated_userlist_model.dart';
class UserApi{



  Future<PaginatedUserListModel?> GetPaginatedUserList(String? my_url) async {
   
    Response res = await get(Uri.parse(my_url !.toString()  ),headers: {
      //'Authorization': 'Bearer $tokenUser'
 "Accept": "application/json",

    });

    if (res.statusCode == 200) {
//print("done get Pagination subcategory");
      var paginated_userList =   PaginatedUserListModel.fromJson(jsonDecode(res.body));

      return paginated_userList;
    } else {
     print("Error in get Pagination userlist");
    print('the error in get Pagination userlist '+res.body);
    }
  }

 


  
  Future<UserDetailsModel?> GetUserDetailsById(int ?id) async {

    Response res = await get(Uri.parse( "https://reqres.in/api/users/"+'$id'  ),headers: {

    });
    if (res.statusCode == 200) {
      var info =   UserDetailsModel.fromJson(jsonDecode(res.body));
      return info;
    } else {
      print('error is'+res.body);
    }
  }
}
